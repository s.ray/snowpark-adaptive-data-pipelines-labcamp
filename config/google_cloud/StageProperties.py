class StageProperties:
   """Get Google Cloud Storage Stage properties."""

   def __init__(
      self,
      storage_integration_name: str = "LABCAMP_GCS",
      bucket: str = "reply-labcamp",
      path_lab1: str = "snowpark-adaptive-data-pipelines-labcamp/laboratory1/data/in",
      path_lab2: str = "snowpark-adaptive-data-pipelines-labcamp/laboratory2/data/in",
      path_lab3: str = "snowpark-adaptive-data-pipelines-labcamp/laboratory3/data/in",
      stage_name_lab1: str = "stage_lab1",
      stage_name_lab2: str = "stage_lab2",
      stage_name_lab3: str = "stage_lab3",
   ) -> None:

        self.storage_integration_name = storage_integration_name
        self.bucket = bucket
        self.path_lab1 = path_lab1
        self.path_lab2 = path_lab2
        self.path_lab3 = path_lab3
        self.stage_name_lab1 = stage_name_lab1
        self.stage_name_lab2 = stage_name_lab2
        self.stage_name_lab3 = stage_name_lab3

   def get_gcs_properties(self) -> dict:

      stage_properties = {
         "storage_integration_name": self.storage_integration_name,
         "bucket": self.bucket,
         "path_lab1": self.path_lab1,
         "path_lab2": self.path_lab2,
         "path_lab3": self.path_lab3,
         "stage_name_lab1": self.stage_name_lab1,
         "stage_name_lab2": self.stage_name_lab2,
         "stage_name_lab3": self.stage_name_lab3
      }

      return stage_properties
