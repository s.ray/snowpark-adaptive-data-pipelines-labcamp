from config.snowflake.SnowflakeUser import SnowflakeUser

class SnowflakeSessionProperties:
    """Get Snowpark Session connection properties."""

    def __init__(
        self,
        account: str = "iq16394.eu-central-1",
        user: str = "LABCAMP_{user_id}",
        password: str = "LABCAMP_{user_id}",
        role: str = "LABCAMP_ROLE",
        database: str = "LABCAMP",
        schema: str = "LABCAMP_SCHEMA_{user_id}",
        warehouse: str = "LABCAMP_WH_{warehouse_id}",
    ) -> None:

        self.account = account
        self.user = user
        self.password = password
        self.role = role
        self.database = database
        self.schema = schema
        self.warehouse = warehouse

    def get_snowflake_connection_properties(self) -> dict:
        
        user = SnowflakeUser()
        user_id = user.user_id
        warehouse_id = user.warehouse_id

        snowflake_connection_properties = {
            "account": self.account,
            "user": self.user.replace("{user_id}", user_id),
            "password": self.password.replace("{user_id}", user_id),
            "role": self.role,
            "database": self.database,
            "schema": self.schema.replace("{user_id}", user_id),
            "warehouse": self.warehouse.replace("{warehouse_id}", warehouse_id),
        }

        return snowflake_connection_properties
