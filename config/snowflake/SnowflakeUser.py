class SnowflakeUser:
    def __init__(
        self,
        user_id: str = "USER00",
        warehouse_id: str = "00",
    ) -> None:
        """
        Get Snowflake User info.
        Replace the following parameter values with the assigned ones:

        :param user_id: Unique identifier for the Labcamp User.
        :param warehouse_id: Unique identifier for the Snowflake Warehouse.
        """

        self.user_id = user_id
        self.warehouse_id = warehouse_id
