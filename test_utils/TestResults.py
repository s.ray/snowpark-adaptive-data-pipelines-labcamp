from typing import Any, Callable

from snowflake.snowpark import DataFrame
from snowflake.snowpark.functions import col, max, min

class TestResults:
    """Utility class for automatically testing laboratory results."""

    def __init__(self) -> None:
        pass

    @staticmethod
    def run_test(
        df: DataFrame,
        f: Callable,
        expected: Any,
    ) -> None:
        """
        Generic method for running tests against DataFrame results.

        :param df: DataFrame object for running the test.
        :param f: Function executed against the DataFrame object.
        :param expected: Expected result for passing the test.
        """

        actual = f(df)
        assert actual == expected, f"Test Failed!\n\nExpected: {expected}\n\nGot: {actual}"
        print("Test Passed.")
    
    def test_count(
        self,
        df: DataFrame,
        records_number: int,
    ) -> None:
        """Generic method for testing table counts."""
        self.run_test(
            df,
            lambda df:df.count(),
            records_number,
        )
    
    def test_schema(
        self,
        df: DataFrame,
        struct_schema: str,
    ) -> None:
        """Generic method for testing table schema and data types."""
        self.run_test(
            df,
            lambda df:str(df.schema),
            struct_schema,
        )

    def test_count_df_rows(
        self,
        df: DataFrame,
        records_number: int,
    ) -> None:
        """Generic method for testing DataFrame records count."""
        self.run_test(
            df,
            lambda df:len(df),
            records_number,
        )
        
    def test_count_df_columns(
        self,
        df: DataFrame,
        columns_number: int,
    ) -> None:
        """Generic method for testing DataFrame columns count."""
        self.run_test(
            df,
            lambda df:len(df.columns),
            columns_number,
        )
        
    def test_top_position_avg(
        self,
        df: DataFrame,
        top_position_avg: float,
    ) -> None:

        self.run_test(
            df,
            lambda df:float(df.collect()[0][1]),
            top_position_avg,
        )
        
    def test_max_cost(
        self,
        df: DataFrame,
        max_cost: int,
    ) -> None:

        self.run_test(
            df,
            lambda df:df.collect()[0][0],
            max_cost,
        )
        
    def test_max_cost_league(
        self,
        df: DataFrame,
        max_cost_league: int,
    ) -> None:
    
        self.run_test(
            df,
            lambda df:df.collect()[0][1],
            max_cost_league,
        )
    
    def test_max_value(
        self,
        df: DataFrame,
        column_name: str,
        max_value_expected: Any,
    ) -> None:
        """Generic method for testing Max value in a given DataFrame column."""

        self.run_test(
            df,
            lambda df:str(df.agg(max(col(column_name))).collect()[0][f"MAX({column_name})"]),
            max_value_expected,
        )
    
    def test_min_value(
        self,
        df: DataFrame,
        column_name: str,
        min_value_expected: Any,
    ) -> None:
        """Generic method for testing Min value in a given DataFrame column."""
        
        self.run_test(
            df,
            lambda df:str(df.agg(min(col(column_name))).collect()[0][f"MIN({column_name})"]),
            min_value_expected,
        )
