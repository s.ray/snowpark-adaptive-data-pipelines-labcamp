-- Create Google Cloud Storage Integration with or without allowed locations / paths.

CREATE STORAGE INTEGRATION LABCAMP_GCS
  TYPE = EXTERNAL_STAGE
  STORAGE_PROVIDER = 'GCS'
  ENABLED = TRUE
  STORAGE_ALLOWED_LOCATIONS = ('gcs://reply-labcamp/snowpark-adaptive-data-pipelines-labcamp/');

-- Retrieve the identifier of the Google Cloud Service Account to be used by Snowflake.

DESC STORAGE INTEGRATION LABCAMP_GCS;
-- kuluqzlpkl@sfc-eufrankfurt-1-g4a.iam.gserviceaccount.com

-- In the Google Cloud Console, grant the Service Account permissions to access bucket objects:
-- storage.buckets.get
-- storage.objects.create
-- storage.objects.get
-- storage.objects.list