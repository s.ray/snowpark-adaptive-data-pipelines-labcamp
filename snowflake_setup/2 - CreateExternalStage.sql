USE DATABASE LABCAMP;

-- Create Google Cloud Storage Stage with Storage Integration.

CREATE STAGE snowpark_adaptive_data_pipelines_labcamp
  url = 'gcs://reply-labcamp/snowpark-adaptive-data-pipelines-labcamp/'
  storage_integration = LABCAMP_GCS;