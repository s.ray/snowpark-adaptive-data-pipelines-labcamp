# How To Build Adaptive and Elastic Data Pipelines

[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
[![Version](https://img.shields.io/badge/version-1.0.0-orange)](https://img.shields.io/badge/version-1.0.0-orange)

## **Usage Notes**

### **Labcamp Introduction**

The goal of this **How to Build Adaptive and Elastic Data Pipelines** Labcamp is to show how to build adaptive and elastic Data Pipelines with **Snowflake**, a modern Cloud Data Platform, by eliminating the complexity, cost and limitations of traditional Data Architectures.

In this session you will learn:

- Snowflake Cloud Data Platform: its innovative architecture and some of the main features of the platform;
- How to build data pipelines in Snowflake;
- How to leverage Snowpark and how to deploy distributed Spark workloads and execute them within Snowflake.

### **Labcamp Preparatory Material**

Snowflake & Snowpark:
- https://docs.snowflake.com/en/user-guide-intro.html
- https://www.snowflake.com/snowpark/
- https://www.snowflake.com/blog/snowpark-python-innovation-available-all-snowflake-customers/
- https://docs.snowflake.com/en/developer-guide/snowpark/index.html
- https://docs.snowflake.com/en/developer-guide/snowpark/python/index.html
- https://docs.snowflake.com/en/developer-guide/snowpark/reference/python/index.html

Anaconda & Jupyter:
- https://www.anaconda.com/products/distribution
- https://docs.jupyter.org/en/latest/

Spark (DataFrame APIs):
- https://spark.apache.org/docs/latest/index.html
- https://spark.apache.org/docs/latest/api/python/
- https://spark.apache.org/docs/latest/api/python/getting_started/quickstart_df.html

 ### **Labcamp Requirements**

For the scope of this laboratory, it is required to download and install **[Miniconda](https://docs.conda.io/en/latest/miniconda.html#windows-installers)**, with bundled Python 3.8.

Miniconda is a free minimal installer for conda, which also includes Python and some other packages. During the Labcamp, we are going to create and activate a conda virtual environment, that will be deleted at the end of the session.

Moreover, it is strongly recommended to have installed **[Vistual Studio Code](https://code.visualstudio.com/)** as an IDE for running the labs.

### **Local Development and Testing Environment**:

Follow the instructions below to setup your virtual environment with conda and to install the needed dependencies and python packages.

**Step N°1**: Create and activate a Python Virtual Environment named "**20221020-reply-labcamp-py38**" with Python **3.8**.

```
conda create --name 20221020-reply-labcamp-py38 -c https://repo.anaconda.com/pkgs/snowflake python=3.8

conda activate 20221020-reply-labcamp-py38
```

**Step N°2**: Install needed packaged with pip.

```
pip install snowflake-snowpark-python
pip install matplotlib
pip install notebook
pip install "snowflake-connector-python[pandas]"
```

### **Laboratory Users and Compute Cluster**

During the course of this Labcamp, you are going to use a technical user for connecting to Snowflake and a dedicated Compute Cluster (Virtual Warehouse) for running Spark transformations.

Before starting with the labs, make sure to assign one of the available users to your e-mail in the following **Google Sheet**:

- https://docs.google.com/spreadsheets/d/1CVe2o4hL6iiI6VanrPxxAgve4V7fQmDUNd7HrkXzcyI/edit?usp=sharing

Take note of the dedicate **Compute Cluster (Virtual Warehouse)** for your user.

Open [SnowflakeUser](config/snowflake/SnowflakeUser.py) and set "**user_id**" and "**warehouse_id**" parameters with your personal ones and save.

You can also access the **Snowflake WebUI** via the following URL in order to see how the different operations and transformations are applied and to inspect the Database Schema components associated with your "user_id":

- https://app.snowflake.com/eu-central-1/iq16394

In the login page, enter the User Name indicated in the Google Sheet and use the same identifier also for the password field.

### **Starting a Jupiter Notebook**

If you have installed Visual Studio Code, it is possible to run Laboratory Notebooks cells and manage kernels directly through the IDE.

In the case you don't have VSC installed, make sure to activate a Jupyter Browser session via the following terminal command:

```
jupyter notebook
```

### **Laboratory N°1**

In this laboratory:

- Reading data from two CSV files (Book Reviews and Book Details) and loading data into Snowflake Staging Tables;

- Performing some pre-processing and normalization of the data in the source structures;

- Joining the two tables together and retrieving aggregates (Top Books by Authors and Category).

![Lab1](/docs/lab1/lab1.png)

### **Laboratory N°2**

In this laboratory:

- Read data from a JSON file in an external staging area, writing in Snowflake table;

- Switch from Snowpark DataFrame to Pandas DataFrame in order to make some transformation;

- Using Snowpark DataFrame in order to query data in Snowflake tables.

![Lab2](/docs/lab2/lab2.png)

### **Laboratory N°3**

In this laboratory:

- Reading data from two Parquet files (Bitcoin and Ethereum Historical Market Prices and Volumes) and loading data into Snowflake Staging Tables;

- Joining the two tables together and performing some additional transformations;

- Analyse data and plot results.

![Lab1](/docs/lab3/lab3.png)

### **Ending the Labcamp**

At the end of the Labcamp, it is possible to remove the utilised conda environment through the following terminal command:

```
conda deactivate
conda env remove --name 20221020-reply-labcamp-py38
```